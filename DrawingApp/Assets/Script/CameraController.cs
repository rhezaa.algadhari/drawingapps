﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Vector2 curDist;
    Vector2 prevDist;

    Camera mainCamera;
    Vector3 cameraInitialPosition;

    public float zoomSpeed = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount >= 2)
        {
            //proses 2 jari gerak
            if(Input.GetTouch(0).phase==TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                curDist = Input.GetTouch(0).position - Input.GetTouch(1).position; // current distance between 2 fingers
                prevDist = ((Input.GetTouch(0).position - Input.GetTouch(1).position) - (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition)); //difference in previous locations using delta pos  
                float touchDelta = curDist.magnitude - prevDist.magnitude;

                //ubah ukuran kamera
                mainCamera.orthographicSize -= touchDelta + zoomSpeed * Time.deltaTime;
                mainCamera.orthographicSize = Mathf.Max(mainCamera.orthographicSize, 0, 1f);
                transform.Translate(new Vector2(-Input.GetTouch(0).deltaPosition.x * Time.deltaTime, -Input.GetTouch(0).deltaPosition.y * Time.deltaTime));
            }
        }
    }
}
