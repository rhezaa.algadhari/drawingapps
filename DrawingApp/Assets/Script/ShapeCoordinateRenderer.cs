﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShapeCoordinateRenderer : MonoBehaviour
{

    public GameObject textPrefab;

    public Painter painter;

    public MeshFilter drawMeshFilter;

    public MeshRenderer drawRenderer;

    List<GameObject> vertexPositions = new List<GameObject>();

    public int i, j;

    // Start is called before the first frame update
    void Start()
    {
        textPrefab.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowVertexPositions()
    {
        ClearVertexPositions();

        for (int i = 0; i < painter.ShapeModels.Count; i++)
        {
            for (int j = 0; j < painter.ShapeModels[i].Vertices.Count; j++)
            {
                var vertex = painter.ShapeModels[i].Vertices[j];

                // membuat objek teks baru
                GameObject newText = Instantiate(textPrefab) as GameObject;
                var tmp = newText.GetComponent<TextMeshPro>();
                string formattedString = string.Format("({0:0.00},{1:0.00})", vertex.x, vertex.y);
                tmp.SetText(formattedString);
                newText.SetActive(true);

                // mengatur posisi teks
                Vector3 worldPos = GetQuadPos(vertex);
                newText.transform.position = new Vector3(worldPos.x, worldPos.y, newText.transform.position.z);

                vertexPositions.Add(newText);
            }
        }
    }

    // menghapus objek teks di scene
    void ClearVertexPositions()
    {
        for (int i = 0; i < vertexPositions.Count; i++)
        {
            Destroy(vertexPositions[i]);
        }
        vertexPositions.Clear();
    }

    Vector2 GetQuadPos(Vector2 screenpos)
    {
        //get vertex quad
        Vector3[] vertices = drawMeshFilter.mesh.vertices;
        Vector3 bottomLeft = drawMeshFilter.transform.TransformPoint(vertices[0]);
        Vector3 topRight = drawMeshFilter.transform.TransformPoint(vertices[1]);

        //besar quad
        float width = Mathf.Abs(bottomLeft.x - topRight.x);
        float height = Mathf.Abs(topRight.y - bottomLeft.y);

        //mengubah koordinat dari 0 hinga 1 menjadi relatif
        // mengubah koordinat menjadi relatif dari 0 hingga 1
        float uv_x = (screenpos.x / (float)drawRenderer.material.mainTexture.width);
        float uv_y = (screenpos.y / (float)drawRenderer.material.mainTexture.height);

        //mendapatkan posisi titik relatif quad
        float x = (width * uv_x) + bottomLeft.x;
        float y = (height * uv_y) + bottomLeft.y;
        return new Vector2(x, y);
    }
}
